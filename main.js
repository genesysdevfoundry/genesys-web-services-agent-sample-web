var _ = require("underscore");
var angular = require("angular");

angular.module("agent", [])
  .controller("AgentController", ["$scope", "$http", function($scope, $http) {
    var agent = this;
    var subscribedToApiEvents = false;
    
    agent.startSession = function(username, password) {
      $http({
        method: "GET",
        url: "/api/v2/me?subresources=*",
        headers: {
          "Authorization": "Basic " + btoa(username + ":" + password)
        }
      }).then(function successCallback(response) {
          agent.user = response.data.user;
          
          if (!subscribedToApiEvents) {
            var faye = require("faye");
            var client = new faye.Client("/api/v2/notifications", options = {});
            client.disable("websocket");
            client.subscribe("/**", onEvent);
            subscribedToApiEvents = true;
          }
          
          postOp($http, "/api/v2/me",
            {
              "operationName": "StartContactCenterSession",
              "channels": ["voice", "email", "chat"]
            }
          );
        }, function errorCallback(response) {
          alert("Failed to retrieve agent state");
        });
    };

    agent.endSession = function() {
      postOp($http, "/api/v2/me", { "operationName": "EndContactCenterSession" });
    };

    agent.ready = function() {
      postOp($http, "/api/v2/me", { "operationName": "Ready" });
    };

    agent.notReady = function() {
      postOp($http, "/api/v2/me", { "operationName": "NotReady" });
    };

    function onEvent(e) {
      for (var key in e) {
        agent.user[key] = e[key];
        
        if (key == "call") {
          var call = agent.user[key];
          
          var callIndex = _.findIndex(agent.user.calls, function(c) { return c.id == call.id; });
          if (call.state == "Released") {
            if (callIndex >= 0) {
              agent.user.calls.splice(callIndex, 1);
            }
          } else {
            if (callIndex >= 0) {
              agent.user.calls[callIndex] = call;
            } else {
              agent.user.calls.push(call);
            }
          }
        }
      }
      $scope.$apply();
    }
  }])
  .controller("CallController", ["$scope", "$http", function($scope, $http) {
    var call = this;

    call.answer = function() {
      postOp($http, "/api/v2/me" + $scope.call.path, { "operationName": "Answer" });
    }

    call.hangUp = function() {
      postOp($http, "/api/v2/me" + $scope.call.path, { "operationName": "Hangup" });
    }
    
    call.enabledOp = function(opName) {
      return $scope.call.capabilities.indexOf(opName) < 0;
    }
  }]);

function postOp($http, url, data) {
  $http({
    method: "POST",
    url: url,
    data: data
  }).then(function successCallback(response) {
    }, function errorCallback(response) {
      alert("Failed to perform operation " + data.operationName);
    });
}
