Genesys Web Services Agent Sample Web Application
=================================================

This sample illustrates how to easily create a Genesys Web Services application for the browser.

Building the sample
-------------------

You will need Node.js and NPM for to build the sample.

As usual for NPM packages, for installing all dependencies, open a command-line,
go to the project's root directory, and run

```
npm install
```

The JavaScript source code of this sample is in `main.js`.
Run the `build` custom script in order to build the `bundle.js` file.
(You will need to run the `build` script every time you make changes to `main.js`).

```
npm run build
```

Running the sample
------------------

This sample assumes that the sample web page and the Genesys Web Services server
are in the same domain.

For convenience, in order to test locally, you can run a local web server that serves
local files and will delegate to the Genesys Web Services server when needed.
If you want to run your tests this way, modify the `proxy` script inside `package.json`,
in order to point to your Genesys Web Services URL. At the project's root directory 
(where `index.html` is located) run:

```
npm run proxy
```

Now you can open a browser window pointing to

```
http://localhost:8080/index.html
```